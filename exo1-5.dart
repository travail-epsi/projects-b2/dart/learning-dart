import 'NE_PAS_TOUCHER/user_input.dart';
import 'game.dart';
import 'settings.dart';

void main() {
  print(
      "Bonjour aventurier ! Bienvenue dans la quête des 7 clés ! Que voulez vous faire ?");
  displayMenu(name: askName());
}

void displayMenu({String name = "Mongolo"}) {
  var check = false, choice, end = true;
  do {

    do {
      print("1: Partir à l'aventure");
      print("2: Préférences");
      print("3: Quitter");
      print("");
      choice = readInt("Quel est votre choix ?");
      check = checkInput(choice);
    } while (!check);

    end = menu(choice, name: name);
  } while (end);
}

bool menu(int choice, {String name = "Mongolo"}) {
  switch (choice) {
    case 1:
      print("Je vous envoie dans une quête épique ! ${name}");
      game(name: name);
      return true;
    case 2:
      print("Vous ne pouvez rien changer dommage");
      settings();
      return true;
    case 3:
      print("Sans vous ${name} la magie ne reviendra pas ! Dommage");
      return false;
    default:
      print("Vous aviez le choix entre 1, 2 et 3 c'est pas si compliqué si ?");
      return true;
  }
}

bool checkInput(int choice) {
  if (choice < 1 && choice > 3) return false;
  return true;
}

String askName() {
  var name;
  do {
    name = readText("Quel est ton nom d'aventurier ?");
    if (name.length < 3) {
      print("Votre nom est beaucoup trop court pour qu'on s'en rappelle ! ");
    }
  } while (name.length < 3);

  return name;
}
